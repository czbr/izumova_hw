#!/usr/bin/env sh

echo "build success"

VER=$(cat VERSION).${BUILD_NUMBER}

mkdir -p result
echo 'some artifacts (txt)' > result/res-${VER}.txt
echo 'some artifacts (json)' > result/res-${VER}.json
